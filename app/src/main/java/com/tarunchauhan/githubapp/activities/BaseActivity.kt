package com.tarunchauhan.githubapp.activities

import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.FrameLayout
import com.tarunchauhan.githubapp.R
import kotlinx.android.synthetic.main.activity_base.*

/**
 * Created by pchub on 28-02-2018.
 */
open class BaseActivity : AppCompatActivity() {

//--------------------------------------------------------------------------------

    override fun setContentView(layoutResID: Int) {

        val view = layoutInflater.inflate(R.layout.activity_base, null)

        val frameLayout = view.findViewById<FrameLayout>(R.id.activity_container)

        layoutInflater.inflate(layoutResID, frameLayout, true)

        super.setContentView(view)
    }

//--------------------------------------------------------------------------------

    /**
     * This method will display as progress bar
     * which is not cancelable.
     */
    fun showProgress() {
        v_overlay.visibility = View.VISIBLE
        progress_bar.visibility = View.VISIBLE
    }

//--------------------------------------------------------------------------------

    /**
     * This method will hide the progress bar which was
     * displayed by the [showProgress] method.
     */
    fun hideProgress() {
        v_overlay.visibility = View.GONE
        progress_bar.visibility = View.GONE
    }

//--------------------------------------------------------------------------------
}