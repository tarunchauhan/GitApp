package com.tarunchauhan.githubapp.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.tarunchauhan.githubapp.R
import com.tarunchauhan.githubapp.adapters.RepositoryAdapter
import com.tarunchauhan.githubapp.models.GitHubRepository
import com.tarunchauhan.githubapp.models.GitHubUser
import com.tarunchauhan.githubapp.models.UserViewModel
import kotlinx.android.synthetic.main.activity_contributor.*

/**
 * Created by pchub on 28-02-2018.
 */
class ContributorActivity : BaseActivity() {

    private var userViewModel: UserViewModel? = null

    private var userReposAdapter: RepositoryAdapter? = null

//--------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contributor)

        // Add back button in toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // Get RepositoryViewModel
        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)

        // Set selected repository to view model if is not set
        if (userViewModel?.getUser()?.username?.isNotBlank() != true)
            userViewModel?.setUser(intent.getParcelableExtra("contributor"))

        // Add observer for contributors list
        userViewModel?.getUserRepos()?.observe(this, userReposObserver)

        // Add Observer for Alert Message from View Model
        userViewModel?.getAlertMessage()?.observe(this, alertMessageObserver)

        // Add Observer for Loading status
        userViewModel?.getIsLoading()?.observe(this, loadingStatusObserver)

        // set data
        setData()

        rv_repo.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {

                if (dy > 0 && userViewModel?.getIsLoading()?.value == false) {

                    val mLayoutManager = rv_repo.layoutManager as LinearLayoutManager
                    val visibleItemsCount = mLayoutManager.childCount
                    val totalItemsCount = mLayoutManager.itemCount
                    val pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition()


                    if ((visibleItemsCount + pastVisibleItems) >= totalItemsCount) {
                        userViewModel?.loadUserRepos()
                    }

                }
            }
        })

    }

//--------------------------------------------------------------------------------

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

//--------------------------------------------------------------------------------

    private fun setData() {

        val user = userViewModel?.getUser() ?: GitHubUser()

        Glide.with(iv_avatar)
                .load(user.avatar_url)
                .into(iv_avatar)

        tv_name.text = user.username

        rv_repo.layoutManager = LinearLayoutManager(this)

        userReposAdapter = RepositoryAdapter(userViewModel?.getUserRepos()?.value ?: arrayListOf(), userReposAdapterListener)
        rv_repo.adapter = userReposAdapter
    }

//--------------------------------------------------------------------------------

    private val alertMessageObserver = Observer<String> {
        Log.d("log", this@ContributorActivity::class.java.simpleName + " received Alert message : $it")
        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
    }

//--------------------------------------------------------------------------------

    private val userReposObserver = Observer<ArrayList<GitHubRepository>> {

        // it : ArrayList<GitHubRepository>
        userReposAdapter?.updateData(it ?: arrayListOf())


        if (it?.isNotEmpty() != true) {
            tv_noReposFound.visibility = View.VISIBLE
        } else {
            tv_noReposFound.visibility = View.GONE
        }

    }

//--------------------------------------------------------------------------------

    private val loadingStatusObserver = Observer<Boolean> {
        if (it == true) {
            showProgress()
        } else if (it == false) {
            hideProgress()
        }
    }

//--------------------------------------------------------------------------------

    private val userReposAdapterListener = object : RepositoryAdapter.RepositoryAdapterListener {
        override fun onRepositorySelected(repository: GitHubRepository) {

            val intent = Intent(this@ContributorActivity, RepoDetailsActivity::class.java)
            intent.putExtra("repository", repository)
            startActivity(intent)
        }
    }

//--------------------------------------------------------------------------------
}