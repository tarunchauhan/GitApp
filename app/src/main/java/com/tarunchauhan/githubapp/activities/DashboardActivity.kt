package com.tarunchauhan.githubapp.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.tarunchauhan.githubapp.R
import com.tarunchauhan.githubapp.adapters.RepositoryAdapter
import com.tarunchauhan.githubapp.dialogs.FiltersDialog
import com.tarunchauhan.githubapp.models.GitHubRepository
import com.tarunchauhan.githubapp.models.SearchRepositoriesViewModel
import kotlinx.android.synthetic.main.activity_dashboard.*


class DashboardActivity : BaseActivity(), FiltersDialog.FiltersDialogListener {

    lateinit var repositoriesModel: SearchRepositoriesViewModel

    private var repoAdapter: RepositoryAdapter? = null

//--------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        // Init view model
        repositoriesModel = ViewModelProviders.of(this).get(SearchRepositoriesViewModel::class.java)

        // Add Observer on repositories list
        repositoriesModel.getRepositories().observe(this, repositoryObserver)

        // Add Observer for Alert Message from View Model
        repositoriesModel.getAlertMessage().observe(this, alertMessageObserver)

        // Add Observer for Loading status
        repositoriesModel.getIsLoading().observe(this, loadingStatusObserver)

        // Set values from view model
        setValues()

        // Implement Infinite Scrolling
        rv_repo.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {

                if (dy > 0 && repositoriesModel.getIsLoading().value == false) {

                    val mLayoutManager = rv_repo.layoutManager as LinearLayoutManager
                    val visibleItemsCount = mLayoutManager.childCount
                    val totalItemsCount = mLayoutManager.itemCount
                    val pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition()


                    if ((visibleItemsCount + pastVisibleItems) >= totalItemsCount) {
                        repositoriesModel.loadMoreRepositories()
                    }

                }
            }
        })

        // Listen for Search IME button to search
        et_search.setOnEditorActionListener({ v: TextView?, actionId: Int, event: KeyEvent? ->

            var handled = false

            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event?.action == KeyEvent.ACTION_DOWN) {

                // Perform search action
                performSearch()

                handled = true
            }

            // Return true if you have consumed the action, else false.
            handled
        })

        // Listen for focus change of EditText to hide/show search button
        et_search.setOnFocusChangeListener({ v: View?, hasFocus: Boolean ->

            if (hasFocus) {
                ib_search.visibility = View.VISIBLE
            } else {
                ib_search.visibility = View.GONE
            }

        })

        // Search button click listener
        ib_search.setOnClickListener({ performSearch() })

        // Filters click listener
        ib_filter.setOnClickListener({ showFilters() })
    }

//--------------------------------------------------------------------------------

    override fun onFiltersApply() {

        // Perform search if there is any search query defined
        if (repositoriesModel.getSearchQuery().isNotBlank()) {
            performSearch()
        }
    }

//--------------------------------------------------------------------------------

    private val loadingStatusObserver = Observer<Boolean> {
        if (it == true) {
            showProgress()
        } else if (it == false) {
            hideProgress()
        }
    }

//--------------------------------------------------------------------------------

    private val alertMessageObserver = Observer<String> {
        Log.d("log", this@DashboardActivity::class.java.simpleName + " received Alert message : $it")
        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
    }

//--------------------------------------------------------------------------------

    private val repositoryObserver = Observer<ArrayList<GitHubRepository>> {

        // it : ArrayList<SearchResponse>
        repoAdapter?.updateData(it ?: arrayListOf())

        if (it?.isNotEmpty() != true) {
            tv_noRepoFound.visibility = View.VISIBLE
        } else {
            tv_noRepoFound.visibility = View.GONE
        }
    }

//--------------------------------------------------------------------------------

    private val repositoryAdapterListener = object : RepositoryAdapter.RepositoryAdapterListener {
        override fun onRepositorySelected(repository: GitHubRepository) {

            val intent = Intent(this@DashboardActivity, RepoDetailsActivity::class.java)

            intent.putExtra("repository", repository)

            startActivity(intent)
        }
    }

//--------------------------------------------------------------------------------

    private fun setValues() {

        et_search.setText(repositoriesModel.getSearchQuery())

        rv_repo.layoutManager = LinearLayoutManager(this)

        repoAdapter = RepositoryAdapter(repositoriesModel.getRepositories().value ?: arrayListOf(), repositoryAdapterListener)
        rv_repo.adapter = repoAdapter


        if (repositoriesModel.getRepositories().value?.isNotEmpty() != true) {
            if (repositoriesModel.getSearchQuery().isBlank()) {
                tv_searchToStart.visibility = View.VISIBLE
            } else {
                tv_searchToStart.visibility = View.GONE
                repositoriesModel.searchRepositories(et_search.text.toString())
            }
        }

    }

//--------------------------------------------------------------------------------

    private fun performSearch() {

        // Check if search to start is visible. IF yes then hide it.
        if (tv_searchToStart.visibility == View.VISIBLE) {
            tv_searchToStart.visibility = View.GONE
        }

        // Remove focus from search bar as its job is done!
        et_search.clearFocus()

        if (et_search.text.toString().isBlank()) {
            // Show error message if search is empty
            Toast.makeText(this@DashboardActivity, "Search Can't be empty", Toast.LENGTH_SHORT).show()

            // Show current search text
            et_search.setText(repositoriesModel.getSearchQuery())

        } else {
            // call api
            repositoriesModel.searchRepositories(et_search.text.toString())
        }

        // hide virtual keyboard
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(et_search.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN)
    }

//--------------------------------------------------------------------------------

    private fun showFilters() {
        FiltersDialog().show(supportFragmentManager, "Filters Dialog")
    }

//--------------------------------------------------------------------------------
}
