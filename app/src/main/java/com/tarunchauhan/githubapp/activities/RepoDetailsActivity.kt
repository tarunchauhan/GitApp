package com.tarunchauhan.githubapp.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tarunchauhan.githubapp.R
import com.tarunchauhan.githubapp.adapters.ContributorsAdapter
import com.tarunchauhan.githubapp.models.GitHubRepository
import com.tarunchauhan.githubapp.models.GitHubUser
import com.tarunchauhan.githubapp.models.RepositoryViewModel
import kotlinx.android.synthetic.main.activity_repo_details.*

/**
 * Created by pchub on 28-02-2018.
 */
class RepoDetailsActivity : BaseActivity() {

    private var repoViewModel: RepositoryViewModel? = null

    private var contributorsAdapter: ContributorsAdapter? = null

//--------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_details)

        // Add back button in toolbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // Get RepositoryViewModel
        repoViewModel = ViewModelProviders.of(this).get(RepositoryViewModel::class.java)

        // Set selected repository to view model if is not set
        if (repoViewModel?.getRepository()?.name?.isNotBlank() != true)
            repoViewModel?.setRepository(intent.getParcelableExtra("repository"))

        // Add observer for contributors list
        repoViewModel?.getContributors()?.observe(this, contributorsObserver)

        // Add Observer for Alert Message from View Model
        repoViewModel?.getAlertMessage()?.observe(this, alertMessageObserver)

        // Add Observer for Loading status
        repoViewModel?.getIsLoading()?.observe(this, loadingStatusObserver)

        // set data
        setData()

        rv_contributors.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {

                if (dy > 0 && repoViewModel?.getIsLoading()?.value == false) {

                    val mLayoutManager = rv_contributors.layoutManager as LinearLayoutManager
                    val visibleItemsCount = mLayoutManager.childCount
                    val totalItemsCount = mLayoutManager.itemCount
                    val pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition()


                    if ((visibleItemsCount + pastVisibleItems) >= totalItemsCount) {
                        repoViewModel?.loadContributors()
                    }

                }
            }
        })

        tv_repo_link.setOnClickListener({
            val intent = Intent(this@RepoDetailsActivity, WebViewActivity::class.java)
            intent.putExtra("url", repoViewModel?.getRepository()?.html_url)
            intent.putExtra("title", repoViewModel?.getRepository()?.name)
            startActivity(intent)
        })
    }

//--------------------------------------------------------------------------------

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

//--------------------------------------------------------------------------------

    private fun setData() {

        val repository = repoViewModel?.getRepository() ?: GitHubRepository()
        tv_repo_name.text = repository.name

        Glide.with(iv_avatar)
                .load(repository.owner.avatar_url)
                .apply(RequestOptions.circleCropTransform())
                .into(iv_avatar)

        supportActionBar?.title = repository.name

        tv_description.text = getString(R.string.description, repository.description)
        tv_description.movementMethod = ScrollingMovementMethod()

        rv_contributors.layoutManager = LinearLayoutManager(this)

        contributorsAdapter = ContributorsAdapter(repoViewModel?.getContributors()?.value ?: arrayListOf(), contributorAdapterListener)
        rv_contributors.adapter = contributorsAdapter
    }

//--------------------------------------------------------------------------------

    private val loadingStatusObserver = Observer<Boolean> {
        if (it == true) {
            showProgress()
        } else if (it == false) {
            hideProgress()
        }
    }

//--------------------------------------------------------------------------------

    private val alertMessageObserver = Observer<String> {
        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
    }

//--------------------------------------------------------------------------------

    private val contributorsObserver = Observer<ArrayList<GitHubUser>> {

        // it : ArrayList<GitHubUser>

        contributorsAdapter?.updateData(it ?: arrayListOf())

        if (it?.isNotEmpty() != true) {
            tv_noContributorsFound.visibility = View.VISIBLE
        } else {
            tv_noContributorsFound.visibility = View.GONE
        }
    }

//--------------------------------------------------------------------------------

    private val contributorAdapterListener = object : ContributorsAdapter.ContributorsAdapterListener {
        override fun onContributorSelected(contributor: GitHubUser) {

            val intent = Intent(this@RepoDetailsActivity, ContributorActivity::class.java)
            intent.putExtra("contributor", contributor)
            startActivity(intent)

        }
    }

//--------------------------------------------------------------------------------
}