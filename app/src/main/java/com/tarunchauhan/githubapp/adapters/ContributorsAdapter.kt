package com.tarunchauhan.githubapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.tarunchauhan.githubapp.R
import com.tarunchauhan.githubapp.models.GitHubUser

/**
 * Created by pchub on 01-03-2018.
 */
class ContributorsAdapter(private var contributors: ArrayList<GitHubUser>, private val adapterListener: ContributorsAdapterListener) : RecyclerView.Adapter<ContributorsAdapter.ViewHolder>() {

//--------------------------------------------------------------------------------

    interface ContributorsAdapterListener {
        fun onContributorSelected(contributor: GitHubUser)
    }

//--------------------------------------------------------------------------------

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_contributor, parent, false))
    }

//--------------------------------------------------------------------------------

    override fun getItemCount(): Int = contributors.size

//--------------------------------------------------------------------------------

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val contributor = contributors[position]

        Glide.with(holder.avatar)
                .load(contributor.avatar_url)
                .apply(RequestOptions.bitmapTransform(CircleCrop()))
                .into(holder.avatar)

        holder.name.text = contributor.username
    }

//--------------------------------------------------------------------------------

    fun updateData(contributors: ArrayList<GitHubUser>) {
        this.contributors = contributors
        notifyDataSetChanged()
    }

//--------------------------------------------------------------------------------

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val avatar: ImageView = itemView.findViewById(R.id.iv_contributor_avatar)
        val name: TextView = itemView.findViewById(R.id.tv_contributor_name)

        init {
            itemView.setOnClickListener({
                adapterListener.onContributorSelected(contributors[adapterPosition])
            })
        }
    }

//--------------------------------------------------------------------------------
}