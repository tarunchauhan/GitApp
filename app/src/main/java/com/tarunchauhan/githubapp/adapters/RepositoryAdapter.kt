package com.tarunchauhan.githubapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tarunchauhan.githubapp.R
import com.tarunchauhan.githubapp.models.GitHubRepository

/**
 * Created by pchub on 28-02-2018.
 */
class RepositoryAdapter(private var repositories: ArrayList<GitHubRepository>, private val adapterListener: RepositoryAdapterListener) : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

//--------------------------------------------------------------------------------

    interface RepositoryAdapterListener {
        fun onRepositorySelected(repository: GitHubRepository)
    }

//--------------------------------------------------------------------------------

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_repo, parent, false))
    }

//--------------------------------------------------------------------------------

    override fun getItemCount(): Int = repositories.size

//--------------------------------------------------------------------------------

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val repository = repositories[position]

        Glide.with(holder.avatar)
                .load(repository.owner.avatar_url)
                .into(holder.avatar)

        holder.name.text = repository.name
        holder.fullName.text = repository.full_name
        holder.watcherCount.text = holder.watcherCount.context.getString(R.string.watcher_count, repository.watchers_count)

    }

//--------------------------------------------------------------------------------

    fun updateData(repositories: ArrayList<GitHubRepository>) {
        this.repositories = repositories
        notifyDataSetChanged()
    }

//--------------------------------------------------------------------------------

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val avatar: ImageView = itemView.findViewById(R.id.iv_avatar)
        val name: TextView = itemView.findViewById(R.id.tv_name)
        val fullName: TextView = itemView.findViewById(R.id.tv_full_name)
        val watcherCount: TextView = itemView.findViewById(R.id.tv_watcher_count)

        init {
            itemView.setOnClickListener({
                adapterListener.onRepositorySelected(repositories[adapterPosition])
            })
        }
    }

//--------------------------------------------------------------------------------
}