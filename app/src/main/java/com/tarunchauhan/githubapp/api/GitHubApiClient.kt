package com.tarunchauhan.githubapp.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by pchub on 28-02-2018.
 */
object GitHubApiClient {

//--------------------------------------------------------------------------------

    private val BASE_URL = "https://api.github.com/"

    private var retrofit: Retrofit? = null

    fun getClient(): Retrofit {

        if (retrofit == null) {

            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }

        return retrofit!!
    }

//--------------------------------------------------------------------------------
}