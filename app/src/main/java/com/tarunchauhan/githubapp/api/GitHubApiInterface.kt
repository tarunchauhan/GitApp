package com.tarunchauhan.githubapp.api

import com.tarunchauhan.githubapp.models.GitHubRepository
import com.tarunchauhan.githubapp.models.GitHubUser
import com.tarunchauhan.githubapp.models.SearchResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by pchub on 28-02-2018.
 */
interface GitHubApiInterface {

//--------------------------------------------------------------------------------

    @GET("search/repositories")
    fun searchRepos(
            @Query("q") searchQuery: String,
            @Query("sort") sort: String = "",
            @Query("order") order: String = "",
            @Query("page") pageNumber: Int,
            @Query("per_page") perPage: Int = 10
    ): Call<SearchResponse>

//--------------------------------------------------------------------------------

    @GET("repos/{username}/{repo_name}/contributors")
    fun getContributors(
            @Path("username") username: String,
            @Path("repo_name") repoName: String,
            @Query("page") pageNumber: Int,
            @Query("per_page") perPage: Int = 10
    ): Call<ArrayList<GitHubUser>>

//--------------------------------------------------------------------------------
    
    @GET("users/{username}/repos")
    fun getReposOfUser(
            @Path("username") username: String,
            @Query("page") pageNumber: Int,
            @Query("per_page") perPage: Int = 10
    ): Call<ArrayList<GitHubRepository>>

//--------------------------------------------------------------------------------
}