package com.tarunchauhan.githubapp.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.DatePicker
import android.widget.RadioButton
import com.tarunchauhan.githubapp.R
import com.tarunchauhan.githubapp.models.SearchRepositoriesViewModel
import kotlinx.android.synthetic.main.dialog_filters.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by pchub on 28-02-2018.
 */
class FiltersDialog : DialogFragment(), View.OnClickListener {

    interface FiltersDialogListener {
        fun onFiltersApply()
    }

//--------------------------------------------------------------------------------

    private var listener: FiltersDialogListener? = null

    private var dashboardViewModel: SearchRepositoriesViewModel? = null

    private val fromDate = Calendar.getInstance()
    private val toDate = Calendar.getInstance()

//--------------------------------------------------------------------------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {

            listener = activity as FiltersDialogListener

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

//--------------------------------------------------------------------------------

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        // Remove title to avoid weird blue line above dialog in lower api devices
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        // Remove background color of dialog to make it transparent
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // Return dialog
        return dialog
    }

//--------------------------------------------------------------------------------

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_filters, container, false)
    }

//--------------------------------------------------------------------------------

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val dashboardActivity = activity

        dashboardViewModel =
                if (dashboardActivity != null)
                    ViewModelProviders.of(dashboardActivity).get(SearchRepositoriesViewModel::class.java)
                else
                    null


        setValues()

        // Set click listeners
        tv_fromDate.setOnClickListener(this)
        tv_toDate.setOnClickListener(this)
        tv_addFilters.setOnClickListener(this)
        tv_cancel.setOnClickListener(this)
    }

//--------------------------------------------------------------------------------

    override fun onClick(v: View?) {

        when (v) {

            tv_fromDate -> {
                // Launch Date Picker and Get Date
                getDatePicker(isFromDate = true)
            }

            tv_toDate -> {
                // Launch Date Picker and Get Date
                getDatePicker(isFromDate = false)
            }

            tv_addFilters -> {

                // Call interface and apply the filters
                applyFilters()

            }

            tv_cancel -> {
                this.dismiss()
            }
        }

    }

//--------------------------------------------------------------------------------

    private fun setValues() {

        rg_sort.findViewWithTag<RadioButton>(dashboardViewModel?.getSort())?.isChecked = true

        val dateRange = dashboardViewModel?.getDateRange() ?: ""
        if (dateRange.contains("..")) {
            val dates = dateRange.split("..")

            if (dates[0] != "*") {
                tv_fromDate.text = dates[0]
                fromDate.time = getDateFromString(dates[0])
            }

            if (dates[1] != "*") {
                tv_toDate.text = dates[1]
                toDate.time = getDateFromString(dates[1])
            }
        }

        rg_orderBy.findViewWithTag<RadioButton>(dashboardViewModel?.getOrder())?.isChecked = true
    }

//--------------------------------------------------------------------------------


    private fun getDatePicker(isFromDate: Boolean) {

        val dialog = DatePickerDialog(context, { _: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->

            val calendar = if (isFromDate) {
                fromDate
            } else {
                toDate
            }

            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            if (isFromDate) {
                tv_fromDate.text = getStringFromDate(fromDate.time)
            } else {
                tv_toDate.text = getStringFromDate(toDate.time)
            }

        }, toDate.get(Calendar.YEAR), toDate.get(Calendar.MONTH), toDate.get(Calendar.DAY_OF_MONTH))

        if (isFromDate) {
            dialog.datePicker.maxDate = toDate.timeInMillis
        } else {
            if (tv_fromDate.text.toString() != getString(R.string.from)) {
                dialog.datePicker.minDate = fromDate.timeInMillis
            }
            dialog.datePicker.maxDate = Date().time
        }

        dialog.show()
    }


//--------------------------------------------------------------------------------

    private fun applyFilters() {

        // Update values in view model
        val selectedSortItem = rg_sort.findViewById<RadioButton>(rg_sort.checkedRadioButtonId)
        dashboardViewModel?.setSort(selectedSortItem?.tag?.toString() ?: "")

        val selectedOrderItem = rg_orderBy.findViewById<RadioButton>(rg_orderBy.checkedRadioButtonId)
        dashboardViewModel?.setOrder(selectedOrderItem?.tag?.toString() ?: "")

        dashboardViewModel?.setDateRange(getDateRange())

        // Notify activity that filters are applied
        listener?.onFiltersApply()

        // Dismiss filters dialog
        this.dismiss()

    }

//--------------------------------------------------------------------------------

    private fun getDateRange(): String {

        val fromFormattedDate = if (tv_fromDate.text.toString() == getString(R.string.from)) {
            "*"
        } else {
            tv_fromDate.text.toString()
        }

        val toFormattedDate = if (tv_toDate.text.toString() == getString(R.string.to)) {
            "*"
        } else {
            tv_toDate.text.toString()
        }

        val range = "$fromFormattedDate..$toFormattedDate"

        return if (range != "*..*") range else ""
    }

//--------------------------------------------------------------------------------

    private fun getStringFromDate(inputDate: Date): String {
        return SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(inputDate)
    }

//--------------------------------------------------------------------------------

    private fun getDateFromString(inputDate: String): Date {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        return sdf.parse(inputDate)
    }

//--------------------------------------------------------------------------------
}