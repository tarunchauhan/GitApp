package com.tarunchauhan.githubapp.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by pchub on 28-02-2018.
 */
data class SearchResponse(
        val items: ArrayList<GitHubRepository> = arrayListOf()
)

data class GitHubRepository(
        var id: String = "",
        var name: String = "",
        var full_name: String = "",
        var owner: GitHubUser = GitHubUser(),
        var description: String = "",
        var html_url: String = "",
        var watchers_count: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(GitHubUser::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id ?: "")
        parcel.writeString(name ?: "")
        parcel.writeString(full_name ?: "")
        parcel.writeParcelable(owner, flags)
        parcel.writeString(description ?: "")
        parcel.writeString(html_url ?: "")
        parcel.writeString(watchers_count ?: "")
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GitHubRepository> {
        override fun createFromParcel(parcel: Parcel): GitHubRepository {
            return GitHubRepository(parcel)
        }

        override fun newArray(size: Int): Array<GitHubRepository?> {
            return arrayOfNulls(size)
        }
    }
}

data class GitHubUser(
        @SerializedName("login")
        @Expose
        var username: String = "",
        var id: String = "",
        var avatar_url: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username ?: "")
        parcel.writeString(id ?: "")
        parcel.writeString(avatar_url ?: "")
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GitHubUser> {
        override fun createFromParcel(parcel: Parcel): GitHubUser {
            return GitHubUser(parcel)
        }

        override fun newArray(size: Int): Array<GitHubUser?> {
            return arrayOfNulls(size)
        }
    }
}