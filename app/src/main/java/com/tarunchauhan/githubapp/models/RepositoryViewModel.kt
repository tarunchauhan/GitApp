package com.tarunchauhan.githubapp.models

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.tarunchauhan.githubapp.api.GitHubApiClient
import com.tarunchauhan.githubapp.api.GitHubApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by pchub on 01-03-2018.
 */
class RepositoryViewModel : ViewModel() {

//--------------------------------------------------------------------------------

    private var currentPageNumber = 1
    private val perPage = 10
    private var isLoading: MutableLiveData<Boolean> = MutableLiveData()


    private var repository: GitHubRepository = GitHubRepository()

    private var contributorsList: ArrayList<GitHubUser> = arrayListOf()
    private var contributors: MutableLiveData<ArrayList<GitHubUser>> = MutableLiveData()

    private var alertMessage: MutableLiveData<String> = MutableLiveData()

//--------------------------------------------------------------------------------

    fun getAlertMessage(): LiveData<String> {
        return alertMessage
    }

//--------------------------------------------------------------------------------

    fun getRepository(): GitHubRepository = repository

    fun setRepository(repository: GitHubRepository) {
        this.repository = repository
    }

//--------------------------------------------------------------------------------

    fun getIsLoading(): LiveData<Boolean> {
        return isLoading
    }

//--------------------------------------------------------------------------------

    fun getContributors(): LiveData<ArrayList<GitHubUser>> {

        if (contributors.value?.isNotEmpty() != true) {
            loadContributors()
        }

        return contributors
    }

//--------------------------------------------------------------------------------

    fun loadContributors() {

        // Don't call API again if already loading response
        if (isLoading.value == true) return

        // Change Loading status
        isLoading.value = true

        // Get Retrofit
        val retrofit = GitHubApiClient.getClient()

        // Get Client
        val client = retrofit.create(GitHubApiInterface::class.java)

        // Get Call object of API
        val call = client.getContributors(repository.owner.username, repository.name, currentPageNumber, perPage)

        // Call API
        call.enqueue(object : Callback<ArrayList<GitHubUser>> {
            override fun onResponse(call: Call<ArrayList<GitHubUser>>?, response: Response<ArrayList<GitHubUser>>?) {

                // API finished change loading status
                isLoading.value = false

                if (response?.isSuccessful == true) {
                    currentPageNumber++
                } else {
                    alertMessage.value = response?.message() ?: "Failed to fetch data from server!"
                }

                if (response?.body() != null) {
                    contributorsList.addAll(response.body()!!)
                    contributors.value = contributorsList
                }

            }

            override fun onFailure(call: Call<ArrayList<GitHubUser>>?, t: Throwable?) {
                // Error occurred Change loading status
                isLoading.value = false

                //  Send alert message of error
                alertMessage.value = t?.message ?: "Failed to fetch data from server!"
            }

        })

    }

//--------------------------------------------------------------------------------
}