package com.tarunchauhan.githubapp.models

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.tarunchauhan.githubapp.api.GitHubApiClient
import com.tarunchauhan.githubapp.api.GitHubApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by pchub on 28-02-2018.
 */
class SearchRepositoriesViewModel : ViewModel() {

//--------------------------------------------------------------------------------

    private var currentPageNumber = 1
    private val perPage = 10
    private var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    private var searchQuery = ""
    private var sort = ""
    private var order = ""
    private var dateRange = ""

    private val repos: ArrayList<GitHubRepository> = arrayListOf()

    private var repositories: MutableLiveData<ArrayList<GitHubRepository>> = MutableLiveData()

    private var alertMessage: MutableLiveData<String> = MutableLiveData()

//--------------------------------------------------------------------------------

    fun getAlertMessage(): LiveData<String> {
        return alertMessage
    }

//--------------------------------------------------------------------------------

    fun getRepositories(): LiveData<ArrayList<GitHubRepository>> {
        return repositories
    }

//--------------------------------------------------------------------------------

    fun getIsLoading(): LiveData<Boolean> {
        return isLoading
    }

//--------------------------------------------------------------------------------

    fun getSearchQuery(): String {
        return searchQuery
    }

//--------------------------------------------------------------------------------

    fun getSort(): String {
        return sort
    }

    fun setSort(sort: String) {
        this.sort = sort
    }

//--------------------------------------------------------------------------------

    fun getOrder(): String {
        return order
    }

    fun setOrder(order: String) {
        this.order = order
    }

//--------------------------------------------------------------------------------

    fun getDateRange(): String {
        return dateRange
    }

    fun setDateRange(dateRange: String) {
        this.dateRange = dateRange
    }


//--------------------------------------------------------------------------------

    fun searchRepositories(searchQuery: String) {

        Log.d("log", "Calling API")

        // Update the variables
        this.searchQuery = searchQuery
        this.sort = sort
        this.order = order

        // Change page number back to 1 because new search is made.
        this.currentPageNumber = 1

        // Clear previous data
        repos.clear()

        if (searchQuery.isBlank()) {
            alertMessage.value = "Search can't be empty"
            return
        }

        // Change isLoading to false because user intentionally wants to call API
        isLoading.value = false

        // Make API call
        loadMoreRepositories()
    }

//--------------------------------------------------------------------------------

    fun loadMoreRepositories() {

        // Don't call API again if already loading response
        if (isLoading.value == true) return

        // Change Loading status
        isLoading.value = true

        // Get Retrofit
        val retrofit = GitHubApiClient.getClient()

        // Get Client
        val client = retrofit.create(GitHubApiInterface::class.java)

        // Make Search Query
        val search = if (dateRange.isNotBlank()) {
            this.searchQuery + " created:$dateRange"
        } else {
            searchQuery
        }

        // Get Call object of API
        val call = client.searchRepos(search, sort, order, currentPageNumber, perPage)

        // Call API
        call.enqueue(object : Callback<SearchResponse> {
            override fun onResponse(call: Call<SearchResponse>?, response: Response<SearchResponse>?) {

                // API finished change loading status
                isLoading.value = false

                if (response?.isSuccessful == true) {
                    currentPageNumber++
                } else {
                    alertMessage.value = response?.message() ?: "Failed to fetch data from server!"
                }

                if (response?.body() != null) {
                    repos.addAll(response.body()!!.items)
                    repositories.value = repos
                }
            }

            override fun onFailure(call: Call<SearchResponse>?, t: Throwable?) {
                t?.printStackTrace()

                //  Send alert message of error
                isLoading.value = false

                //  Send alert message of error
                alertMessage.value = t?.message ?: "Failed to fetch data from server!"
            }

        })

    }

//--------------------------------------------------------------------------------
}