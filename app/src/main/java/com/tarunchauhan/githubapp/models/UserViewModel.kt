package com.tarunchauhan.githubapp.models

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.tarunchauhan.githubapp.api.GitHubApiClient
import com.tarunchauhan.githubapp.api.GitHubApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by pchub on 01-03-2018.
 */
class UserViewModel : ViewModel() {

//--------------------------------------------------------------------------------

    private var currentPageNumber = 1
    private val perPage = 10
    private var isLoading: MutableLiveData<Boolean> = MutableLiveData()


    private var user: GitHubUser = GitHubUser()

    private var userRepoList: ArrayList<GitHubRepository> = arrayListOf()
    private var userRepos: MutableLiveData<ArrayList<GitHubRepository>> = MutableLiveData()

    private var alertMessage: MutableLiveData<String> = MutableLiveData()

//--------------------------------------------------------------------------------

    fun getAlertMessage(): LiveData<String> {
        return alertMessage
    }

//--------------------------------------------------------------------------------

    fun getIsLoading(): LiveData<Boolean> {
        return isLoading
    }

//--------------------------------------------------------------------------------

    fun getUser(): GitHubUser = user

    fun setUser(user: GitHubUser) {
        this.user = user
    }

//--------------------------------------------------------------------------------

    fun getUserRepos(): LiveData<ArrayList<GitHubRepository>> {

        if (userRepoList.isEmpty()) {
            loadUserRepos()
        }

        return userRepos
    }

//--------------------------------------------------------------------------------

    fun loadUserRepos() {

        // Don't call API again if already loading response
        if (isLoading.value == true) return

        // Change Loading status
        isLoading.value = true

        // Get Retrofit
        val retrofit = GitHubApiClient.getClient()

        // Get Client
        val client = retrofit.create(GitHubApiInterface::class.java)

        // Get Call object of API
        val call = client.getReposOfUser(user.username, currentPageNumber, perPage)

        // Call API
        call.enqueue(object : Callback<ArrayList<GitHubRepository>> {
            override fun onResponse(call: Call<ArrayList<GitHubRepository>>?, response: Response<ArrayList<GitHubRepository>>?) {

                // API finished change loading status
                isLoading.value = false

                if (response?.isSuccessful == true) {
                    currentPageNumber++
                } else {
                    alertMessage.value = response?.message() ?: "Failed to fetch data from server!"
                }

                if (response?.body() != null) {
                    userRepoList.addAll(response.body()!!)
                    userRepos.value = userRepoList
                }

            }

            override fun onFailure(call: Call<ArrayList<GitHubRepository>>?, t: Throwable?) {

                //  Send alert message of error
                isLoading.value = false

                //  Send alert message of error
                alertMessage.value = t?.message ?: "Failed to fetch data from server!"
            }

        })

    }

//--------------------------------------------------------------------------------
}